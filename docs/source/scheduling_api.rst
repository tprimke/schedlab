Scheduling API
==============

All the functions described here are independent on the scheduling problem. Some of them can be regarded as low-level scheduling routines. In general, all of them use the scheduling object, described in the :doc:`dstructs`.

Machines' state
---------------

.. js:function:: idleAt( machine_id, time, schedule )

   :param string machine_id: The machine's identifier.
   :param number time: The time moment.
   :param schedule: The schedule object.
   :returns: ``true`` or ``false``

Checks, whether the referenced machine is idle at the requested time moment, according to the given schedule object.

Examples:

.. code-block:: javascript

  if ( idleAt('m1',10,sch) ) {
    // do something
  }


.. js:function:: idleFrom( machine_id, time, schedule )

   :param string machine_id: The machine's identifier.
   :param number time: The time moment.
   :param schedule: The schedule object.
   :returns: a number or ``Infinity``

Returns such a time moment, not lower than the given ``time`` parameter, that the machine of the given identifier is idle starting from the ``time``.

If the machine is busy at the given time moment, 0 is returned.

If there are no tasks scheduled on the machine, starting from the given time, ``Infinity`` is returned.


.. js:function:: idleFor( machine_id, time, schedule )

   :param string machine_id: The machine's identifier.
   :param number time: The time moment.
   :param schedule: The schedule object.
   :returns: a number or ``Infinity``

Returns the number of time units, that the given machine is idle for, starting from the given time moment.

If the machine is busy at the given time moment, 0 is returned.

If there are no tasks scheduled on the machine, starting from the given time, ``Infinity`` is returned.


.. js:function:: busyAt( machine_id, time, schedule )

   :param string machine_id: The machine's identifier.
   :param number time: The time moment.
   :param schedule: The schedule object.
   :returns: ``true`` or ``false``

Checks, whether the referenced machine is busy at the requested time moment, according to the given schedule object.


.. js:function:: busyFrom( machine_id, time, schedule )

   :param string machine_id: The machine's identifier.
   :param number time: The time moment.
   :param schedule: The schedule object.
   :returns: a number of time units

Returns the number of time units, that the given machine is busy for, starting from the given time moment.

If the machine is idle at the given time moment, 0 is returned.


.. js:function:: busyFor( machine_id, time, schedule )

   :param string machine_id: The machine's identifier.
   :param number time: The time moment.
   :param schedule: The schedule object.
   :returns: a number of time units

Returns the number of time units, that the given machine is busy for, starting from the given time moment.

If the machine is idle at the given time moment, 0 is returned.


.. js:function:: machineIdleFrom( machine_id, schedule )

   :param string machine_id: The machine's identifier.
   :param schedule: The schedule object.
   :returns: a time moment

Returns the time moment, at which the machine is idle and there are no scheduled tasks afterwards.

If there are no scheduled tasks on the machine at all, 0 is returned.


.. js:function:: machineIdleTo( machine_id, schedule )

   :param string machine_id: The machine's identifier.
   :param schedule: The schedule object.
   :returns: a time moment or ``Infinity``

Returns the time moment, when the first task is scheduled on the machine.

If there are no tasks scheduled on the machine, ``Infinity`` is returned.


.. js:function:: machineBusyFrom( machine_id, schedule )

   :param string machine_id: The machine's identifier.
   :param schedule: The schedule object.
   :returns: a time moment or ``null``

Returns the time moment, when the first task scheduled on the machine starts.

If there are no scheduled tasks on the machine, ``null`` is returned.


.. js:function:: machineBusyTo( machine_id, schedule )

   :param string machine_id: The machine's identifier.
   :param schedule: The schedule object.
   :returns: a time moment

Returns the time moment, when the last task scheduled on the machine ends.

If there are no scheduled tasks on the machine, 0 is returned.


.. js:function:: taskAt( machine_id, time, schedule )

   :param string machine_id: The machine's identifier.
   :param number time: The time moment.
   :param schedule: The schedule object.
   :returns: the scheduled task object, or ``null``

If there is a scheduled task, on the machine, at the time moment, this task's object is returned. Otherwise, the function returns ``null``.


Scheduling
----------


.. js:function:: schedule( task_id, machine_id, start, end, schedule )

   :param string task_id: The task's identifier.
   :param string machine_id: The machine's identifier.
   :param number start: The time moment to start the task.
   :param number end: The time moment to finish the task.
   :param schedule: The schedule object.
   :returns: A new schedule object.

Scheduled the given task, on the given machine, at the given start moment, to the given end moment, using the given schedule object. Returns a new schedule object, with the scheduled task.

It should be noted, that the passed schedule object is not modified in any way. Instead, a new schedule object is created, and returned.

This function doesn't perform any checks, whether the given task can be scheduled. If this is some other task, that occupies the requested machine at the requested time, the returned new schedule object simply won't be valid.


.. js:function:: safeSchedule( task_id, machine_id, start, end, schedule )

   :param string task_id: The task's identifier.
   :param string machine_id: The machine's identifier.
   :param number start: The time moment to start the task.
   :param number end: The time moment to finish the task.
   :param schedule: The schedule object.
   :returns: A new schedule object, or the passed schedule object.

This function tries to schedule the given task, on the given machine, at the given time.

If it is possible (there aren't any conflicting scheduled tasks), the task is scheduled, and a new schedule object is returned.

If it is not possible (there is some conflicting scheduled task), the task is *not* scheduled, and the original schedule object is returned.

Thus, this function does *safe* scheduling: it will schedule the task, if it is safe to do it.

In other words, this function is equivalent to:

.. code-block:: javascript

  return ( taskAt(machine_id, start) === null ) ?
    schedule(task_id, machine_id, start, end, sched) :
    sched;


.. js:function:: forceSchedule( task_id, machine_id, start, end, schedule )

   :param string task_id: The task's identifier.
   :param string machine_id: The machine's identifier.
   :param number start: The time moment to start the task.
   :param number end: The time moment to finish the task.
   :param schedule: The schedule object.
   :returns: A new schedule object.

This function schedules the given task, on the given machine, at the given time. If there is any conflicting scheduled task, it will me *moved* right, so the obtained (and returned) new schedule won't consist of any conflicting tasks.

In another words, this function is equivalent to:

.. code-block:: javascript

  let task = taskAt(machine_id, start);
  if ( task !== null )
    sched = moveFwd(machine_id, task.start, end-task.start, sched);
  
  return schedule(task_id, machine_id, start, end, sched);


.. js:function:: gentleSchedule( task_id, machine_id, start, end, schedule )

   :param string task_id: The task's identifier.
   :param string machine_id: The machine's identifier.
   :param number start: The time moment to start the task.
   :param number end: The time moment to finish the task.
   :param schedule: The schedule object.
   :returns: A new schedule object.

This function tries to schedule the given task, on the given machine, at the given time moment. It it's not possible (due to a conflicting scheduled task), it looks for a later time moment to schedule the given task.

In other words, this function is going to schedule the given task, as soon as possible, but no sooner than the given time moment.

After the scheduling, the new schedule object is returned.


.. js:function:: removeTask( task_id, schedule )

   :param string task_id: The task's identifier.
   :param schedule: The schedule object.
   :returns: A new schedule object.

Removes the requested task from the schedule, and returns a new schedule object.


.. js:function:: isScheduled( task_id, schedule )

   :param string task_id: The task's identifier.
   :param schedule: The schedule object.
   :returns: ``true`` or ``false``

Checks, whether the requested task is scheduled.


.. js:function:: moveFwd( machine_id, time, change, schedule )

   :param string machine_id: The machine's identifier.
   :param number time: The time moment to start the modification at.
   :param number change: The number of time units to move the scheduled tasks.
   :param schedule: The schedule object.
   :returns: A new schedule object.

Moves all the scheduled tasks, on the machine, starting from the requested time moment, for the requested number of time units into the future. Returns a new schedule object.


.. js:function:: moveBack( machine_id, time, change, schedule )

   :param string machine_id: The machine's identifier.
   :param number time: The time moment to start the modification at.
   :param number change: The number of time units to move the scheduled tasks.
   :param schedule: The schedule object.
   :returns: A new schedule object.

Moves all the scheduled tasks, on the machine, staring from the requested time moment, for the requested number of time units into the past. Returns a new schedule object.

Please note, that it's possible, that the new schedule will consist of some conflicting tasks.


.. js:function:: safeMoveBack( machine_id, time, change, schedule )

   :param string machine_id: The machine's identifier.
   :param number time: The time moment to start the modification at.
   :param number change: The number of time units to move the scheduled tasks.
   :param schedule: The schedule object.
   :returns: A new schedule object, or the passed schedule object.

Tries to move all the scheduled tasks, on the machine, staring from the requested time moment, for the requested number of time units into the past. If it is possible (no conflicting tasks are obtained), returns a new schedule object. If it's not possible (some conflicts would've been introduced), returns the original, unchanged schedule object.


.. js:function:: gentleMoveBack( machine_id, time, change, schedule )

   :param string machine_id: The machine's identifier.
   :param number time: The time moment to start the modification at.
   :param number change: The number of time units to move the scheduled tasks.
   :param schedule: The schedule object.
   :returns: A new schedule object.

Tries to move all the scheduled tasks, on the machine, staring from the requested time moment, for the requested number of time units into the past. If it is not possible (some conflicts would've been introduced), moves the scheduled tasks for such a number of time units, that no conflicts will be introduced. Returns a new schedule object.


.. js:function:: scheduledTasks( pattern, schedule )

   :param string pattern: The scheduled tasks' pattern.
   :param schedule: The schedule object.
   :returns: A collection of scheduled tasks.

This function can be used to obtain a collection of scheduled tasks, whos identifiers conform to the specified pattern.

Examples:

.. code-block:: javascript

  // find all the scheduled tasks
  scheduledTasks('*', schedule);

  // find all the scheduled tasks from the 1st job
  scheduledTasks('1-*', schedule);

