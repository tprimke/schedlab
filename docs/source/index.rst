.. Scheduling Laboratory documentation master file, created by
   sphinx-quickstart on Tue Jul 21 09:31:04 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Scheduling Laboratory's documentation!
=================================================

Contents:

.. toctree::
   :maxdepth: 2

   dstructs
   scheduling_api



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

